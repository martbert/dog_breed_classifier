# Classifying dog breeds the lean and fast way

This repository is meant as little exercise in learning transfer using Keras to build a lean and fast classifier for dog breeds. We invite the reader to take a look at [dog_breeds.ipynb](dog_breeds.ipynb) to get some details on the approach (below you will find the text of this notebook). To run the notebook and utilize the provided utilities, please install the requirements in your Python 3.6+ environment:

    pip install -r requirements.txt

# dog_breeds.ipynb content

We here wish to tackle the dog breeds classification problem. We will specifically focus on two objectives:

1. Make a lean and lightweight classifier that still manages above 60% accuracy.
2. Explore pruning and weighting filters outputed by pre-trained Convolutional Neural Networks architectures amenable to transfer learning.


```python
%load_ext autoreload
%autoreload 2
```


```python
from tqdm import tqdm
import numpy as np
from matplotlib import pyplot as pl
%matplotlib inline
import seaborn as sb
sb.set(font_scale=1.4, style='ticks')

from utils.data import DogBreedData
from utils.model import AttentiveModel
```

## Loading the data for exploration

It should be noted that `DogBreedData` once intialize will look in the git root for a data directory containing images and pre-calculated features. Images and annotations will be downloaded from [http://vision.stanford.edu/aditya86/ImageNetDogs/](http://vision.stanford.edu/aditya86/ImageNetDogs/) if required and image features will be calculated using a pre-trained [SqueezeNet](https://github.com/rcmalli/keras-squeezenet) if not already done. The latter architecture was chosen for it's small footprint and high accuracy on the ImageNet task ([https://arxiv.org/pdf/1602.07360.pdf](https://arxiv.org/pdf/1602.07360.pdf)) such that it is feasible to quickly explore and iterate on a local machine.

The SqueezeNet implementation in Keras outputs, without the classifier head, a 13x13x512 feature tensor from the so-called 9th *Fire* unit. It is debatable whether this is the optimal layer to work with for transfer learning. But the goal not being to reach state-of-the-art accuracy, we will not explore other options.

Each feature tensor was pre-calculated for all dog images, reshaped to 169x512, and stored in a HDF5 file handled by `DogBreedData`.


```python
# Data handling class
dbd = DogBreedData(use_annotations=True, kmodel='SqueezeNet', im_size=227, batch_size=256)
```


```python
# Handles on the features and targets contained in a HDF5 file (uncompressed)
features = dbd.fh['features']
targets = dbd.fh['targets']
n = features.shape[0]
```

## Exploring filter activation

The SqueezeNet model used to calculate the image features was initially trained on the ImageNet dataset that comprises images accross a 1000 categories. Amongst those, funnily enough we have multiple dog breeds. Nevertheless, we're interested in the general concept of transfer learning towards classifying specifically the dog breeds.

Here we take a look at the (mean) activation of the 512 filters to try and see if we can select those more relevant to dogs. Let us focus our attention on those filters whose activation is on average stronger and whose variability accross images is greater.


```python
# The mean activation for all filters accross all images
mean_act = np.array([x.mean(axis=0) for x in features])
```


```python
# Standard deviation of mean activation and average of the mean
# for all filters
X_std_act = mean_act.std(axis=0)
X_avg_act = mean_act.mean(axis=0)
```


```python
# Let us consider values greater than a given threshold
mask_std = X_std_act > 2*X_std_act.std()
mask_avg = X_avg_act > 2*X_avg_act.std()
```


```python
# We combine both masks
mask = mask_std & mask_avg
print('{} filters will be considered'.format(mask.sum()))
```

    149 filters will be considered

```python
fig, ax = pl.subplots()
ax.plot(X_std_act, label='Variability', alpha=0.7)
ax.plot(X_avg_act, label='Strength', alpha=0.7)
ax.plot(np.where(mask)[0], X_avg_act[mask], 'o', label='Selected')
ax.set_xlabel('Filter')
ax.set_title('Exploring filter activation')
ax.legend(loc='best')
```

![png](figures/filters_activations.png)

For the model below, we will use only that subset of filters whose average activation is strong and whose variability is indicative of discrimination power.

## Can attention help?

Let us build a simple model to classify dog breeds based on the features extracted from SqueezeNet further reduced by using the subset of "significant" filters identified above. We will further test the hypothesis that certain filters have, on average, a stronger association with certain dog breeds by introducing filter based attention in our small classifier. This classifier is really quite simple in structure:

![Classifier](figures/classifier.svg)

The attention mechanism was inspired by [https://arxiv.org/abs/1512.08756](https://arxiv.org/abs/1512.08756) and modfied from [Baziotis' Gist entry](https://gist.github.com/cbaziotis/6428df359af27d58078ca5ed9792bd6d). It essentially learns the importance of the filters and weighs them appropriately. It should be know that this simple attention scheme adds only 318 parameters to ~400K initially present.


```python
# The attentive network
m = AttentiveModel(dbd, mask=mask)
```


```python
# Filter attention: is it any useful
hist = {}
basename = 'dog_breeds_model'
for a,a_func in zip([True, False],['Attention','No_Attention']):
    m.clear_session()
    m.build(attention=a, attention_func=a_func)
    m.compile(lr=0.01)
    h = m.train(epochs=40, save_path="{}.{}.h5".format(basename, a_func))
    hist[a_func] = h
```

    Epoch 1/40
    1221/1221 [==============================] - 26s 22ms/step - loss: 2.6238 - acc: 0.3399 - val_loss: 1.6806 - val_acc: 0.5273
    
    ...
    
    Epoch 00039: val_loss improved from 1.10853 to 1.10662, saving model to dog_breeds_model.Attention.h5
    Epoch 40/40
    1221/1221 [==============================] - 29s 24ms/step - loss: 0.8025 - acc: 0.7527 - val_loss: 1.1046 - val_acc: 0.6699
    
    Epoch 00040: val_loss improved from 1.10662 to 1.10456, saving model to dog_breeds_model.Attention.h5
    Epoch 1/40
    1221/1221 [==============================] - 17s 14ms/step - loss: 3.6398 - acc: 0.1477 - val_loss: 2.3576 - val_acc: 0.3682
    
    ...
    
    Epoch 00039: val_loss did not improve
    Epoch 40/40
    1221/1221 [==============================] - 16s 13ms/step - loss: 0.8691 - acc: 0.7279 - val_loss: 1.1494 - val_acc: 0.6689
    
    Epoch 00040: val_loss did not improve



```python
fig,ax = pl.subplots(nrows=1,ncols=2,figsize=(10,4))
ax[0].plot(hist['Attention'].history['val_loss'], label='Attention')
ax[0].plot(hist['No_Attention'].history['val_loss'], label='No attention')
ax[0].legend(loc='best')
ax[0].set_xlabel('Epoch')
ax[0].set_ylabel('Validation loss')

ax[1].plot(hist['Attention'].history['val_acc'], label='Attention')
ax[1].plot(hist['No_Attention'].history['val_acc'], label='No attention')
ax[1].legend(loc='best')
ax[1].set_xlabel('Epoch')
ax[1].set_ylabel('Validation accuracy')

pl.tight_layout()
```

![png](figures/val_loss_acc.png)

As can be seen in the plot above, introducing attention appears to significantly speed-up convergence while marginally improving the loss (and potentially accuracy). Compared to the countless examples of dog classifiers that can be found online (it was a Kaggle challenge after all!), this accuracy might appear disappointing. But with respect to our two initial objectives we have shown that:

1. A fast and relatively lightweight pipeline can be constructed to classify dog breeds with approximately 66% accuracy.
2. The above can be achieved by pruning at least 70% of filters and using trainable attention to weigh the remaining filters.

# Checking the top N

For many applications, the top N accuracy, where N stays relatively modest, might be more relevant than the top 1. Here we report on such a value up to N=10. The good news is that considering only the top 4 already puts us above 90% accuracy.


```python
# Need to reproduce the train, valid split that was used 
# for training the model, hence the random_state=0
from sklearn.model_selection import StratifiedShuffleSplit
sss = StratifiedShuffleSplit(n_splits=1, test_size=0.05, random_state=0)
for train_idx, test_idx in sss.split(targets[:], targets[:]): pass
```


```python
# Get the validation set
idx = list(np.sort(test_idx))
X_valid = features[idx,:,:][:,:,mask]
Y_valid = targets[idx]
```


```python
# Load the previously trained attentive model
m = AttentiveModel(dbd, mask=mask)
m.clear_session()
m.build(attention=True, attention_func='Attention')
m.load('dog_breeds_model.Attention.h5')
```


```python
Y_pred = m.model.predict(X_valid)
idx_top = Y_pred.argsort(axis=1)[:,::-1]
```


```python
acc_topn = np.cumsum(idx_top == np.expand_dims(Y_valid, axis=1),axis=1).mean(axis=0)
```


```python
fig,ax = pl.subplots()
ax.plot(np.arange(10)+1, acc_topn[:10], '--o')
ax.set_xlabel('Top N')
ax.set_ylabel('Accuracy')
```

![png](figures/topn_acc.png)
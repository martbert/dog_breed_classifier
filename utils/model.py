import numpy as np

import keras.backend as K
from keras.layers import Input, Dropout, Dense, Permute
from keras.layers import GlobalAveragePooling1D
from keras.models import Model, load_model
from keras.optimizers import Adagrad
from keras.callbacks import ModelCheckpoint
from utils.attention import AttentionWithContext, Attention

from sklearn.model_selection import StratifiedShuffleSplit

from utils.data import DogBreedData

class AttentiveModel:
    """
    Holds the model and functions necessary to train and predict.
    """
    def __init__(self, dog_breed_data, mask=None):
        self.dog_breed_data = dog_breed_data

        self.features = dog_breed_data.fh['features']
        self.targets = dog_breed_data.fh['targets']

        self.feat_per_filter = self.features.shape[-2]
        self.n_filters = self.features.shape[-1]
        self.n_cat = len(self.dog_breed_data.label_to_int)

        if mask is None:
            self.mask = np.ones(self.n_filters, dtype=int)
        else:
            self.mask = mask
            self.n_filters = self.mask.sum()

    def clear_session(self):
        K.clear_session()

    def build(self, dense_size=512, attention=True, attention_func='Attention'):
        """
        Building the network with or without filter attention.
        """
        inputs = Input(shape=(self.feat_per_filter, self.n_filters))

        if attention:
            permuted = Permute((2,1))(inputs)
            if attention_func == 'Attention':
                weighted = Attention(return_sequences=True)(permuted)
            elif attention_func == 'AttentionWithContext':
                weighted = AttentionWithContext(return_sequences=True)(permuted)
            else:
                raise 'Attention function not recognized'
            weighted = Permute((2,1))(weighted)
        else:
            weighted = inputs
        pooled = GlobalAveragePooling1D()(weighted)

        # Feed forward classifier
        dense_1 = Dense(dense_size, activation='tanh')(pooled)
        dense_1 = Dropout(0.4)(dense_1)
        dense_2 = Dense(dense_size, activation='tanh')(dense_1)
        dense_2 = Dropout(0.4)(dense_2)
        outputs = Dense(self.n_cat, activation='softmax')(dense_2)

        self.model = Model(inputs=inputs, outputs=outputs)

    def compile(self, lr=0.01, init_weigths=True):
        self.model.compile(
            loss='categorical_crossentropy',
            optimizer=Adagrad(lr=lr),
            metrics=['accuracy']
        )

        if init_weigths:
            weights = self.model.get_weights()
            weights = [0.0001*np.random.normal(size=w.shape) for w in weights]
            self.model.set_weights(weights)

    def load(self,path):
        self.model = load_model(path,
            custom_objects={'AttentionWithContext':AttentionWithContext,
                'Attention':Attention})

    def train(self, test_size=0.05, batch_size=16, epochs=15,
        save_path='dog_breeds_model_best.h5', load_best=True):

        # Create the train-valid split
        sss = StratifiedShuffleSplit(n_splits=1, test_size=test_size, random_state=0)
        for train_idx, test_idx in sss.split(self.targets[:], self.targets[:]): pass

        # Initialize generators for train and valid
        train_gen = self.dog_breed_data.batch_generator(
            batch_size=batch_size, indices=train_idx, mask=self.mask)
        valid_gen = self.dog_breed_data.batch_generator(
            batch_size=batch_size, indices=test_idx, mask=self.mask)

        # Checkpointing callback
        callbacks = [
            ModelCheckpoint(
                save_path, 
                monitor='val_loss',
                verbose=1,
                save_best_only=True,
                mode='min')
        ]

        # Fit on generator
        history = self.model.fit_generator(
            train_gen,
            int(train_idx.shape[0] / batch_size),
            epochs=epochs,
            validation_data=valid_gen,
            validation_steps=int(test_idx.shape[0] / batch_size),
            callbacks=callbacks
        )

        # Load best after training
        if load_best:
            self.clear_session()
            self.load(save_path)

        return history

    def get_dog_breeds(self, image_path, topn=1):
        X = self.dog_breed_data.get_image_features(image_path)
        Y = self.model.predict(X)[0]
        idx = np.argsort(Y)
        return [self.dog_breed_data.int_to_label[i] for i in idx[:topn]]

import os, wget, tarfile, glob
import subprocess
from tqdm import tqdm

import numpy as np
import h5py, pickle

import xml.etree.ElementTree as ET

from keras_squeezenet import SqueezeNet
from keras.applications.vgg16 import VGG16
from keras.applications.imagenet_utils import preprocess_input
from keras.preprocessing import image
from PIL import Image as pil_image

MODELS = {'SqueezeNet': SqueezeNet, 'VGG16': VGG16}

def get_git_root():
    """
    Gets git root of a project. If the code is not on git, returns empty string.
    See http://stackoverflow.com/questions/22081209/find-the-root-of-the-git-repository-where-the-file-lives
    """
    try:
        return subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], 
            stdout=subprocess.PIPE).communicate()[0].rstrip().decode("utf-8")
    except:
        return ""

class DogBreedData:
    """
    Class to download and manipulate dog breed images
    """
    def __init__(self, use_annotations=False, im_size=227, batch_size=128,
        kmodel='SqueezeNet'):
        # Params
        self.im_size = im_size
        self.batch_size = batch_size
        self.kmodel = kmodel

        # Root and URLs
        image_url = 'http://vision.stanford.edu/aditya86/ImageNetDogs/images.tar'
        annot_url = 'http://vision.stanford.edu/aditya86/ImageNetDogs/annotation.tar'
        self.git_root = get_git_root()
        
        # Placeholder for model
        self.model = None


        # Check whether data exists, if not download
        self.data_dir = os.path.join(self.git_root, 'data')
        if not os.path.exists(self.data_dir):
            os.mkdir(self.data_dir)

        # Download images if not present
        self.images_path = os.path.join(self.data_dir, 'Images')
        if not os.path.exists(self.images_path):
            wget.download(image_url, out=self.data_dir)
            tar_path = os.path.join(self.data_dir, 'images.tar')
            tar = tarfile.open(tar_path)
            tar.extractall(path=self.data_dir)

        # If use annotations is true, download annotations and use
        self.use_annotations = use_annotations
        if self.use_annotations:
            self.annot_path = os.path.join(self.data_dir, 'Annotation')
            if not os.path.exists(self.annot_path):
                wget.download(annot_url, out=self.data_dir)
                tar_path = os.path.join(self.data_dir, 'annotation.tar')
                tar = tarfile.open(tar_path)
                tar.extractall(path=self.data_dir)
            h5_path = 'features_wannot.h5'
        else:
            h5_path = 'features.h5'

        # Check whether image features exist, if not use VGG16 to 
        # predict said features
        self.features_path = os.path.join(self.data_dir, h5_path)
        if not os.path.exists(self.features_path):
            # Calculate features
            self.fh, self.label_to_int = self.calc_features()

            # Pickle dump the label_to_int
            with open(os.path.join(self.data_dir, 'label_to_int.pckl'), 'wb') as f:
                pickle.dump(self.label_to_int, f)
        else:
            # Load
            self.fh = h5py.File(self.features_path, 'r')

            # Pickle load the label_to_int
            with open(os.path.join(self.data_dir, 'label_to_int.pckl'), 'rb') as f:
                self.label_to_int = pickle.load(f)

        self.n_cat = len(self.label_to_int)

    def close(self):
        # Clean close of the hdf5 file handle
        self.fh.close()

    def _parse_annot_file(self, path):
        """
        Parse a XML annotation to extract it's size and bounding box
        """
        tree = ET.parse(path)
        # Get root
        root = tree.getroot()
        # Size
        size = root.find('size')
        s = []
        for t in ['width','height','depth']:
            s.append(int(size.find(t).text))
        # Object bounding box
        obj = root.find('object')
        bndbox = obj.find('bndbox')
        b = []
        for t in ['xmin','ymin','xmax','ymax']:
            b.append(int(bndbox.find(t).text))
        return s,b

    def get_image_features(self, path):
        if self.model is None:
            self.model = MODELS[self.kmodel](
                input_shape=(self.im_size,self.im_size,3), 
                weights='imagenet', include_top=False)

        # Load image
        img = image.load_img(path, target_size=(self.im_size, self.im_size))
        x = np.expand_dims(image.img_to_array(img), axis=0)
        
        # Get features
        y = self.model.predict(preprocess_input(x))[0]

        return y

    def calc_features(self):
        """
        Pre-calculating the features for each image using a pre-trained
        version of SqueezeNet on the ImageNet dataset. The features are stored
        in a HDF5 file. Filters are flattened, that is we go from:
        (filter_size, filter_size, n_filters) --> (filter_size**2, n_filters)
        """

        # Load DNN
        print('Loading DNN...')
        if self.model is None:
            self.model = MODELS[self.kmodel](
                input_shape=(self.im_size,self.im_size,3), 
                weights='imagenet', include_top=False)
        oshape = tuple(self.model.layers[-1].output_shape)

        # List all image files
        im_paths = glob.glob(self.images_path+'/*/*jpg')
        n_im = len(im_paths)

        # Open hdf5 file for writing
        fh = h5py.File(self.features_path, 'w')

        # Create dataset
        features = fh.create_dataset('features', 
            (n_im,oshape[-2]**2,oshape[-1]),dtype=np.float32)
        targets = fh.create_dataset('targets', (n_im,), dtype=int)

        # Label to int
        label_to_int = {}
        label_count = 0

        # Buffer for batch prediction
        buff = np.zeros((self.batch_size,self.im_size,self.im_size,3), dtype=np.float32)
        cbuff = np.zeros(self.batch_size, dtype=int)

        # For each directory load and images and calculate features
        print('Reading images and calculating features...')
        for idx,p in tqdm(enumerate(im_paths[:n_im]), total=n_im):
            cat = os.path.basename(os.path.dirname(p)).split('-')[1]
            if cat not in label_to_int.keys():
                label_to_int[cat] = label_count
                label_count += 1
            
            m = idx%self.batch_size

            # Load image and get model output
            # Images are buffered to pass them in a batch size to the DNN
            if not self.use_annotations:
                img = image.load_img(p, target_size=(self.im_size, self.im_size))
            else:
                p_annot = os.path.splitext(p.replace('Images','Annotation'))[0]
                img = image.load_img(p)
                size, bndbox = self._parse_annot_file(p_annot)
                # Crop
                img = img.crop(tuple(bndbox))
                # Resize
                img = img.resize((self.im_size, self.im_size), pil_image.NEAREST)
            buff[m] = image.img_to_array(img)
            cbuff[m] = label_to_int[cat]
            if m == self.batch_size-1:
                start = int(idx / self.batch_size)*self.batch_size
                end = start + self.batch_size
                y = self.model.predict(
                    preprocess_input(buff), batch_size=self.batch_size)
                s = y.shape
                y = y.reshape((s[0],s[-2]**2,s[-1]))
                features[start:end,:,:] = y
                targets[start:end] = cbuff
            elif idx == n_im - 1:
                start = int(idx / self.batch_size)*self.batch_size
                y = self.model.predict(
                    preprocess_input(buff[:m+1]), batch_size=self.batch_size)
                s = y.shape
                y = y.reshape((s[0],s[-2]**2,s[-1]))
                features[start:n_im,:,:] = y
                targets[start:n_im] = cbuff[:m+1]

        # Flush the hdf5 buffer to disk
        fh.flush()

        return fh, label_to_int

    def batch_generator(self, batch_size=64, indices=None, mask=None):
        """
        Generate batches from the HDF5 data repository
        """
        # Handles to features and targets
        features = self.fh['features']
        targets = self.fh['targets']

        # One hot encoding matrix
        to_one_hot = np.eye(self.n_cat)

        # Take care of indices
        if indices is None:
            jdx = np.arange(features.shape[0])
        else:
            jdx = indices.copy()
        
        # Number of batches
        nb = int((indices.shape[0]) / batch_size)

        # Deal with a potential mask for filters
        if mask is None:
            mask = np.ones(features.shape[-1], dtype=int)
        else:
            # Make sure we have the proper size
            assert mask.shape[0] == features.shape[-1], "Mask must match features shape"

        while True:
            np.random.shuffle(jdx)
            for i in range(nb):
                start = i*batch_size
                j = list(np.sort(jdx[start:start + batch_size]))
                X = features[j,:,:][:,:,mask]
                Y = to_one_hot[targets[j].astype(int)]

                yield (X, Y)



